# Example: Prepare data for AWS Sagemaker
# image classification

##Purpose:
Most of the examples provided in AWS sagemaker sample notebook folder assume that the data is ready to be fed to the Estimator.

In this example we build a dataset for classification of face images. each training sample would be two face images side-by-side. 

The classifier should predict whether those two images belong to the same person or not. 

The source of images is http://vis-www.cs.umass.edu/lfw/#deepfunnel-anchor

Which includes 3056 pictures from 1657 persons. Some people have only one picture and some have multiple pictures. 


## how to use?

It is assumed that the original images are extracted and uploaded to a buckeet in AWS S3. 

In AWS sagemaker, a notebook instance is created which has full access to the aforemantioned S3 bucket. 

run populate_training_images.ipynb first: 

	This will generate two folders (I know S3 doesn't have folders actually, read as "two sets of images with different prefixes")
	for positive and negative samples. 
	We populate all possibl positive pairs which are 103866 pairs
	We also populate 3% of all possible negative samples which are 273136 pairs
	
Next, run Prepare_training_data.ipynb: 
	
	This will generate chunks of shuffled input samples for an Estimator. Each chunk jas around 3700 samples.
	The reason for such sub-devision is that we don't want to go out of memory on the machine that Sagemaker notebooks are running. 
	At the same time we devide the samples into 8 training folders, one test folder and one validation folder. 

